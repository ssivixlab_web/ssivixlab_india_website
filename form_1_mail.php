<?php 

if(isset($_POST['submit'])){
    $to = "support@ssivixlab.com, admin@ssivixlab.com"; // this is your Email address
    $from = $_POST['input_email']; // this is the sender's Email address
    $first_name = $_POST['input_name'];
    $subject = "MyCLNQ Message";
    //$subject2 = "Copy of your form submission MyCLNQ";
	$message = '<html><body>';
	$message = "<div style='padding-bottom:10px;'>Patient " . strip_tags($_POST['requestfor']) . " request details:</div> \r\n\n";
	$message .= '<table width="100%" align="left" style="border:1px solid #ccc;" rules="all" cellpadding="10">';
	$message .= "<tr style='background: #fff;'><td colspan='2' style='padding-top:10px; padding-bottom:10px; padding-left:10px; padding-right:10px;'><img src='https://ssivixlab.com/myclnqs/doctors/assets/myclnq-logo.png' alt='myclnq Logo' /></td></tr>";
	$message .= "<tr style='background: #fff;'><td width='20%'><strong>Name:</strong> </td><td>" . strip_tags($_POST['input_name']) . "</td></tr>";
	$message .= "<tr style='background: #fff;'><td><strong>Contact Number:</strong> </td><td>" . strip_tags($_POST['input_tel']) . "</td></tr>";
	$message .= "<tr style='background: #fff;'><td width='20%'><strong>Email:</strong> </td><td>" . strip_tags($_POST['input_email']) . "</td></tr>";
	$message .= "<tr style='background: #fff;'><td width='20%'><strong>Date of Birth:</strong> </td><td>" . strip_tags($_POST['dateOfBirth']) . "</td></tr>";
	$message .= "<tr style='background: #fff;'><td width='20%'><strong>Sex:</strong> </td><td>" . strip_tags($_POST['sex']) . "</td></tr>";
	$message .= "<tr style='background: #fff;'><td width='20%'><strong>Request for:</strong> </td><td>" . strip_tags($_POST['requestfor']) . "</td></tr>";
	$message .= "<tr style='background: #fff;'><td width='20%'><strong>Date and Time:</strong> </td><td>" . strip_tags($_POST['input_date_time']) . "</td></tr>";
	$message .= "<tr style='background: #fff;'><td width='20%'><strong>Symptoms:</strong> </td><td>" . strip_tags($_POST['input_symptoms']) . "</td></tr>";
	$message .= "</table>";
	$message .= "</body></html>";
	
	
	$headers = "From: " . $from . "\r\n";
	//$headers .= "Reply-To: ". $email . "\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	$headers .= "X-Priority: 1\r\n";
	mail($to, $subject, $message, $headers);
	
	
	
	
	//$headers = "MIME-Version: 1.0" . "\r\n";
	//$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
	//$headers .= "From:" .$from;
    //$message2 = "Here is the copy of your message " . $first_name . "\n\n" . $_POST['message'];
    //$headers2 = "From:" . $to;
    //mail($to,$subject,$message,$headers);
    //mail($from,$subject2,$message2,$headers2); // sends a copy of the message to the sender
	//echo $message;
    //echo "Mail Sent. Thank you " . $first_name . ", we will contact you shortly.";
    // You can also use header('Location: thank_you.php'); to redirect to another page.
    // You cannot use header and echo together. It's one or the other.
    }

?>
