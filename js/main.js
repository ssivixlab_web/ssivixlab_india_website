(function ($) {
  "use strict";

  // Preloader
  $(window).on('load', function () {
    if ($('#preloader').length) {
      $('#preloader').delay(100).fadeOut('slow', function () {
        $(this).remove();
      });
    }
	
	setTimeout(function(){ 
		//$('#get_theapp_download').modal('show'); 
	}, 5000);
	setTimeout(function(){ 
		$('.boxToolLeft').addClass("active");
	}, 2000);
	$(".closeTool").on("click", function(){
		$('.boxToolLeft').removeClass("active");
	});
	
	$.getJSON('https://api.covid19india.org/data.json', function(data){
		var totalData = data;
		$("#c_no").html(totalData.statewise[0].confirmed);
		$("#a_no").html(totalData.statewise[0].active);
		$("#r_no").html(totalData.statewise[0].recovered);
		$("#d_no").html(totalData.statewise[0].deaths);
		$("#covid19Date").html(totalData.statewise[0].lastupdatedtime);
	});
	
  });
  
  
  /* select Time */
  
	$("#placeYourRequestFormID").validate({
		rules: {
			input_name: {
				required: true
			},
			input_tel: {
				required: true,
				minlength: 10,
				maxlength:10,
				number: true
			},
			input_email: {
				required: true,
				email: true
			},
			dateOfBirth: {
				required: true
			},
			sex: {
				required: true
			},
			requestfor: {
				required: true
			},
			input_date_time: {
				required: true
			},
			input_symptoms: {
				required: true
			}
		},
		messages: {
			input_name: {
				required: "Please enter your name"
			},
			input_tel: {
				required: "Please enter your phone number"
			},
			input_email: {
				required: "Please enter a valid email address"
			},
			dateOfBirth: {
				required: "Please enter your Date of Birth"
			},
			sex: {
				required: "Please select your gender"
			},
			requestfor: {
				required: "Please select anyone field"
			},
			input_date_time: {
				required: "Please enter your Date and Time"
			},
			input_symptoms: {
				required: "Please enter your symptoms"
			}
		},
		errorPlacement: function(label, element) {
                //label.addClass('errorMsq');
                element.parents(".errorMsg").append(label);
            },
		submitHandler: function(form) {
				//$('#placeYourRequestFormID').validate();
				var url = "form_1_mail.php";
				var data = $('#placeYourRequestFormID').serialize();
				$.ajax({
					type: "POST",
					url: url,
					data: data,
					success: function(data) {
						//console.log(data);
						//$("#place_your_request_form").addClass("message_sucess_container");
						var sucess_message = '<div class="sucess_message"><div class="sucess_message_cell"><div class="sucess_message_txt">Thank you ' + $("#input_name").val() + ', We will contact you shortly for booking confirmation. - From MyCLNQ Support Team</div><div id="placeYourRequestFormID_btn" class="message_close_btn" data-dismiss="modal" aria-label="Close">Close( X )</div></div></div>';
						$("#success_message").fadeIn().html(sucess_message);
						$("#placeYourRequestFormID_btn").click(function(){
							$("#success_message").fadeOut();
							//$("#place_your_request_form").removeClass("message_sucess_container");
							var frm = $("#placeYourRequestFormID"); frm.validate().resetForm(); frm[0].reset(); 
						});
					},
					error: function() {alert('failed');}
				});
				return false;
		 }
	});
	$("#place_your_request_form button.close").on("click", function(){
		var frm = $("#placeYourRequestFormID"); frm.validate().resetForm(); frm[0].reset();
	});
  // Back to top button
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('.back-to-top').fadeIn('slow');
    } else {
      $('.back-to-top').fadeOut('slow');
    }
  });
  $('.back-to-top').click(function(){
    $('html, body').animate({scrollTop : 0},1500, 'easeInOutExpo');
    return false;
  });

  // Initiate the wowjs animation library
  //new WOW().init();

  // Initiate superfish on nav menu
  /* $('.nav-menu').superfish({
    animation: {
      opacity: 'show'
    },
    speed: 400
  }); */

  // Mobile Navigation
  if ($('#nav-menu-container').length) {
    var $mobile_nav = $('#nav-menu-container').clone().prop({
      id: 'mobile-nav'
    });
    $mobile_nav.find('> ul').attr({
      'class': '',
      'id': ''
    });
    $('body').append($mobile_nav);
    $('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
    $('body').append('<div id="mobile-body-overly"></div>');
    $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

    $(document).on('click', '.menu-has-children i', function(e) {
      $(this).next().toggleClass('menu-item-active');
      $(this).nextAll('ul').eq(0).slideToggle();
      $(this).toggleClass("fa-chevron-up fa-chevron-down");
    });

    $(document).on('click', '#mobile-nav-toggle', function(e) {
      $('body').toggleClass('mobile-nav-active');
      $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
      $('#mobile-body-overly').toggle();
    });

    $(document).click(function(e) {
      var container = $("#mobile-nav, #mobile-nav-toggle");
      if (!container.is(e.target) && container.has(e.target).length === 0) {
        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').fadeOut();
        }
      }
    });
  } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
    $("#mobile-nav, #mobile-nav-toggle").hide();
  }

  // Header scroll class
  $(window).scroll(function() {
    if ($(this).scrollTop() > 100) {
      $('#header').addClass('header-scrolled');
    } else {
      $('#header').removeClass('header-scrolled');
    }
  });

  if ($(window).scrollTop() > 100) {
    $('#header').addClass('header-scrolled');
  }

  // Smooth scroll for the menu and links with .scrollto classes
  $('.nav-menu a, #mobile-nav a, .scrollto').on('click', function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      if (target.length) {
        var top_space = 0;

        if ($('#header').length) {
          top_space = $('#header').outerHeight();

          if (! $('#header').hasClass('header-scrolled')) {
            top_space = top_space - 20;
          }
        }

        $('html, body').animate({
          scrollTop: target.offset().top - top_space
        }, 1500, 'easeInOutExpo');

        if ($(this).parents('.nav-menu').length) {
          $('.nav-menu .menu-active').removeClass('menu-active');
          $(this).closest('li').addClass('menu-active');
        }

        if ($('body').hasClass('mobile-nav-active')) {
          $('body').removeClass('mobile-nav-active');
          $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
          $('#mobile-body-overly').fadeOut();
        }
        return false;
      }
    }
  });

  // Navigation active state on scroll
  var nav_sections = $('section');
  var main_nav = $('.nav-menu, #mobile-nav');
  var main_nav_height = $('#header').outerHeight();

  $(window).on('scroll', function () {
    var cur_pos = $(this).scrollTop();
  
    nav_sections.each(function() {
      var top = $(this).offset().top - main_nav_height,
          bottom = top + $(this).outerHeight();
  
      if (cur_pos >= top && cur_pos <= bottom) {
        main_nav.find('li').removeClass('menu-active menu-item-active');
        main_nav.find('a[href="#'+$(this).attr('id')+'"]').parent('li').addClass('menu-active menu-item-active');
      }
    });
  });

  // Intro carousel
  var introCarousel = $(".carousel");
  var introCarouselIndicators = $(".carousel-indicators");
  introCarousel.find(".carousel-inner").children(".carousel-item").each(function(index) {
    (index === 0) ?
    introCarouselIndicators.append("<li data-target='#introCarousel' data-slide-to='" + index + "' class='active'></li>") :
    introCarouselIndicators.append("<li data-target='#introCarousel' data-slide-to='" + index + "'></li>");

    $(this).css("background-image", "url('" + $(this).children('.carousel-background').children('img').attr('src') +"')");
    $(this).children('.carousel-background').remove();
  });

  /* $(".carousel").swipe({
    swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
      if (direction == 'left') $(this).carousel('next');
      if (direction == 'right') $(this).carousel('prev');
    },
    allowPageScroll:"vertical"
  }); */

  // Skills section
  /* $('#skills').waypoint(function() {
    $('.progress .progress-bar').each(function() {
      $(this).css("width", $(this).attr("aria-valuenow") + '%');
    });
  }, { offset: '80%'} ); */

  // jQuery counterUp (used in Facts section)
 /*  $('[data-toggle="counter-up"]').counterUp({
    delay: 10,
    time: 1000
  }); */

  // Porfolio isotope and filter
 /*  var portfolioIsotope = $('.portfolio-container').isotope({
    itemSelector: '.portfolio-item',
    layoutMode: 'fitRows'
  });
 */
  $('#portfolio-flters li').on( 'click', function() {
    $("#portfolio-flters li").removeClass('filter-active');
    $(this).addClass('filter-active');

    portfolioIsotope.isotope({ filter: $(this).data('filter') });
  });

  // Clients carousel (uses the Owl Carousel library)
  $(".clients-carousel").owlCarousel({
    autoplay: true,
    dots: true,
    loop: true,
	margin:20,
    responsive: { 0: { items: 2 }, 768: { items: 4 }, 900: { items: 4 }
    }
  });

  // Testimonials carousel (uses the Owl Carousel library)
  $(".service-section-carousel").owlCarousel({
    autoplay: true,
    dots: false,
    loop: true,
	nav:true,
	navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    items: 4,
	reponsiveClass: true,
	 margin:15,
	responsive:{
		0:{
			items:1
		},
		568:{

			items:2
		},
		667:{

			items:2
		},
		768:{

			items:3
		},
		1024:{
			items:4
		}
	}
  });

})(jQuery);

